# Changelog

## Unreleased
- Add Copyright and Credits components
- Clean up and refactor 
- Modified routes to use ESM

## v1.1.0
- Fixed site not being able to open locally via `index.html`
- Upgraded to Webpack v4

## v1.0.0
Initial release
