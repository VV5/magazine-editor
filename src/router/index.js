import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Introduction from '@/components/Introduction'
import PlayerDesk from '@/components/PlayerDesk'
import Research from '@/components/Research'
import ArticleSelection from '@/components/ArticleSelection'
import CoverDesigner from '@/components/CoverDesigner'
import Publish from '@/components/Publish'
import FinalReview from '@/components/FinalReview'
import Credits from '@/components/Credits'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright,
      meta: {
        title: 'Copyright'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/player-desk',
      name: 'PlayerDesk',
      component: PlayerDesk,
      meta: {
        title: 'Player\'s Desk'
      }
    },
    {
      path: '/research',
      name: 'Research',
      component: Research,
      meta: {
        title: 'Research'
      }
    },
    {
      path: '/article-selection',
      name: 'ArticleSelection',
      component: ArticleSelection,
      meta: {
        title: 'Article Selection'
      }
    },
    {
      path: '/cover-designer',
      name: 'CoverDesigner',
      component: CoverDesigner,
      meta: {
        title: 'Cover Designer'
      }
    },
    {
      path: '/publish',
      name: 'Publish',
      component: Publish,
      meta: {
        title: 'Publish'
      }
    },
    {
      path: '/final-review',
      name: 'FinalReview',
      component: FinalReview,
      meta: {
        title: 'Final'
      }
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits,
      meta: {
        title: 'Credits'
      }
    }
  ]
})
