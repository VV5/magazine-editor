import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    deskIcons: [
      {
        id: 1,
        name: 'Research',
        image: require(`@/assets/icons/icon-envelope.svg`),
        isEnabled: true
      },
      {
        id: 2,
        name: 'Article',
        image: require(`@/assets/icons/icon-folder.svg`),
        isEnabled: false
      },
      {
        id: 3,
        name: 'Cover Design',
        image: require(`@/assets/icons/icon-paint-brush.svg`),
        isEnabled: false
      },
      {
        id: 4,
        name: 'Publish',
        image: require(`@/assets/icons/icon-send.svg`),
        isEnabled: false
      }
    ],
    stories: [
      {
        id: 1,
        background: 'intro-lobby.png',
        introductions: [
          {
            id: 1,
            title: 'SSGB',
            text: ' Recognise how the writer\'s use of persuasive language (e.g. choice of words to appeal to authority, novelty or emotions, rhetorical question) varies according to the purpose and audience for writing to achieve impact.'
          },
          {
            id: 2,
            title: 'Learning Objective',
            text: 'To introduce the concepts of pathos, logos and ethos to students through the identification of texts and visuals.'
          }
        ]
      },
      {
        id: 2,
        speech: 'Wow. I\'m here. I\'m actually here.',
        image: require(`@/assets/images/intro-girl-coffee.png`),
        background: 'intro-lobby.png',
        text: '<i>Critical Publishing: Making Magazines for Thinking People</i>. I can\'t believe I\'m actually starting work there. If this is a dream, please please please don\'t let me wake up.'
      },
      {
        id: 3,
        image: require(`@/assets/images/editor.png`),
        background: 'editor-wall.png',
        text: 'My job? Senior editor. I need to pick the right articles so Critical stays the No. 1 magazine publisher in the city. Articles that people want to read. Fresh, relevant, critical.'
      },
      {
        id: 4,
        imageOne: require(`@/assets/images/intro-girl-phone.png`),
        imageTwo: require(`@/assets/images/phone-with-image.png`),
        background: 'editor-wall.png',
        text: 'It\'s Erin, my boss. She\'s cool, but sometimes she\'s just plain cold. Brrrrr. Better get down to her office straightaway.'
      },
      {
        id: 5,
        background: 'boss-at-table.png',
        text: 'So, you\'re the new editor. Hope you\'re up to the task.'
      },
      {
        id: 6,
        background: 'boss-facing.png',
        text: 'Our writing team comes up with ideas for articles. They think they know what people want to read, but sometimes they pick the wrong ideas.'
      },
      {
        id: 7,
        background: 'boss-facing.png',
        text: 'And if nobody reads our magazines, we lose money. It\'s that simple.'
      },
      {
        id: 8,
        background: 'intro-research.png',
        text: 'ERIN: Good thing we\'re not that stupid. We\'ve got a research team looking into what\'s trending every day. Out team\'s out there conducting interviews and discussions. We\'ve got our finger on their pulse.'
      },
      {
        id: 9,
        background: 'intro-assignments.png',
        text: 'ERIN: Your job is to take the articles that our writing team have written and ensure they\'re published in the right magazine. ' +
        'Choose an article unsuitable for that magazine\'s audience, and readership drops. When readership drops, we lose money.'
      },
      {
        id: 10,
        background: 'intro-boss-back.png',
        text: 'ERIN: Well, what are you waiting for? Pick those articles!'
      },
      {
        id: 11,
        background: 'laptop-empty.png'
      }
    ],
    researches: [
      {
        id: 1,
        message: 'Your first assignment is for Worldwide Wanderer, our travel magazine for young people aged 18 - 25. We interviewed three readers. This is what they had to say.',
        emails: {
          young: {
            quote: '“I used to love Worldwide Wanderer, but it’s become worse recently. There are all these articles about expensive holidays that I can’t afford! Tell me about places I can go to on a budget please!"',
            author: 'Summer, student, 21'
          },
          old: {
            quote: '"Worldwide Wanderer? My daughter reads it all the time. She loves adventurous activities, and the magazine gives her ideas for where to go hiking and trekking around the world."',
            author: 'Jerry, advertising agent, 40'
          },
          neutral: {
            quote: '"I’ve never heard of Worldwide Wanderer! Travel magazine? Why would anyone need to travel? Just stay at home!"',
            author: 'Andy, soldier, 32'
          }
        }
      },
      {
        id: 2,
        message: 'Your second assignment is for Health Science Daily. This is a magazine we produce for professionals in the healthcare field. Here are what some readers have to say.',
        emails: {
          young: {
            quote: '"Health Science Daily is my go-to source for learning more about the latest experiments in my field. I do feel that these experiments can be more carefully reported though. Details are important! Who conducted it? Who participated? Can we trust the results?"',
            author: 'Elizabeth, Health Science Researcher, 32'
          },
          old: {
            quote: '"I’ve been in Health Sciences for 32 years and I’ve never read a magazine worse than Health Science Daily. All the articles are so vague and lacking in detail! I’m never going to buy it…well, unless the cover grabs my attention!"',
            author: 'Richard, Health Science Professor, 55'
          },
          neutral: {
            quote: '"Health Science Daily? It’s really boring! Maybe put in some celebrity gossip? I’d buy every single issue!"',
            author: 'Pete, actor, 28'
          }
        }
      },
      {
        id: 3,
        message: 'Your final assignment is for News Weekly, a current affairs and lifestyle magazine. Here’s what some readers have to say.',
        emails: {
          young: {
            quote: '“I’m seriously thinking of cancelling my subscription. The news is always full of depressing, sad content. Why can’t you report on happy things?"',
            author: 'Mandy, accounts executive, 22'
          },
          old: {
            quote: '"I’m very stressed out at work. I’m just looking to read something that will help me to relax and find that joy in life."',
            author: 'Eric, insurance agent, 59'
          },
          neutral: {
            quote: '"I buy every single issue of News Weekly. I don’t read it, though. I just think the covers look nice – I always feel uplifted looking at the pictures!"',
            author: 'Bobby, professional baseball player, 30'
          }
        }
      }
    ],
    magazines: [
      [
        {
          id: 1,
          article: {
            name: 'Off the beaten path in Mongolia: Eleanor Tan tells all',
            content: 'Eleanor Tan, 22, quit her high-class banking job to travel the world. She spent a month in Mongolia, sleeping in yurts (traditional Mongolian huts) and getting around on horseback. ' +
            'Tan’s unconventional form of travel is proving to be popular on social media, with her photos regularly getting over 200 ‘likes’. ' +
            'She writes, ‘to me, travel is about broadening one’s horizons and not about spending the whole day cooped up inside a luxury hotel room. ' +
            'I enjoy getting out there and doing things. In particular, the six day trek I took in the Mongolian countryside, with the sound of hoofbeats across the steppes, is something that I will remember for the rest of my life. It didn’t even cost me that much money!’',
            status: 1,
            response: 'good'
          },
          cover: {
            image: require(`@/assets/images/assignment/mongolia-neutral.png`),
            status: 0,
            response: 'neutral'
          }
        },
        {
          id: 2,
          article: {
            name: 'Enjoy Mongolia!',
            content: 'Mongolia may not seem like the most obvious place for you to take your next trip, but there’s much to enjoy. ' +
            'There is something romantic and exciting about travelling on the vast plains of the Mongolian countryside. ' +
            'Yet, this does not need to be uncomfortable. Kennedy Luxury Tours offers a fantastic five-week tour of Mongolia, ' +
            'which will involve hiking, horseback riding, and canoeing. Prices start from $80,000. ' +
            'Yes, that’s quite expensive, but you get what you pay for – beautiful yurts (traditional Mongolian tents) with soft beds; ' +
            'a cook from one of Mongolia’s best restaurants to make your meals for you; and the latest vehicles for your journey.',
            status: 0,
            response: 'poor'
          },
          cover: {
            image: require(`@/assets/images/assignment/mongolia-poor.png`),
            status: 0,
            response: 'poor'
          }
        },
        {
          id: 3,
          article: {
            name: 'Mongolia: Land of Mystery',
            content: 'Mongolia is the land, of course, of Genghis Khan. Genghis’ fearsome army dominated much of the world in the 13th century. ' +
            'Today, Mongolia is nicknamed ‘The Land of Blue Skies’, with as many as 250 sunny days a year. ' +
            'The vast countryside has changed little since the days of Genghis, and jeep, motorcycle and even horseback tours are very popular among tourists. ' +
            'Activities available include horseback riding, hiking, canoeing and watching the Nadam summer festival.',
            status: 0,
            response: 'neutral'
          },
          cover: {
            image: require(`@/assets/images/assignment/mongolia-good.png`),
            status: 1,
            response: 'good'
          }
        }
      ],
      [
        {
          id: 1,
          article: {
            name: 'Chocolate Good For You…in small doses',
            content: 'A University of Gramogan study recently discovered that chocolate may actually have health benefits. ' +
            'The study measured serotonin levels in 300 participants before and after they ate a bar of chocolate. ' +
            'Serotonin is a chemical that promotes happiness and relaxation. The researchers found that participants’ level of serotonin increased after eating the chocolate. ' +
            'They then repeated the experiment and discovered the same results. Nonetheless, lead researcher Dr Ian Godivsky warns, ' +
            '‘eating too much chocolate may also not be beneficial. Chocolate also contains high amounts of sugar, which may cause diabetes and other health problems in the long run. ' +
            'Like everything else, we advise moderation when consuming any food.’',
            status: 1,
            response: 'good'
          },
          cover: {
            image: require(`@/assets/images/assignment/health-good.png`),
            status: 1,
            response: 'good'
          }
        },
        {
          id: 2,
          article: {
            name: 'Your Mobile Phone Could Kill You',
            content: 'We all love our smartphones in this day and age. But did you know that your mobile phone could kill you? ' +
            'Exploding smartphones are a real thing! Unconfirmed reports have suggested that three people have been hurt by exploding phones over the past year. ' +
            'We don’t know how this happens, but it’s enough for us to be scared. Stop using your mobile phone if you value your life. Stick to regular landlines instead!',
            status: 0,
            response: 'poor'
          },
          cover: {
            image: require(`@/assets/images/assignment/health-neutral.png`),
            status: 0,
            response: 'neutral'
          }
        },
        {
          id: 3,
          article: {
            name: 'Owning Pets Lowers Stress',
            content: 'I have a cat called Ginger. He’s orange, pretty chubby, and enjoys sleeping and eating. In other words, he’s a typical cat. I love him so much. ' +
            'You might ask why I’m writing about Ginger in a health science magazine. The answer’s obvious – owning pets lowers stress. Since I adopted Ginger I’ve found myself less worried about work. I’m sleeping better, eating better, and my work has improved. Those long days at the lab seem all the more manageable now that I have Ginger. ' +
            'So, if you’re stressed out at work, maybe consider adopting a pet cat, dog or rabbit. It will do you wonders!',
            status: 0,
            response: 'neutral'
          },
          cover: {
            image: require(`@/assets/images/assignment/health-poor.png`),
            status: 0,
            response: 'poor'
          }
        }
      ],
      [
        {
          id: 1,
          article: {
            name: 'Terrible Weather Expected to Continue: Experts',
            content: 'Experts have warned that the recent spate of heavy thunderstorms is set to continue for the rest of the month. ' +
            'The unrelenting rain has heaped untold misery onto the nation. The Football Cup tournament has already been cancelled, and the outdoor concert by international megastar singer Rei Chan is also under threat. ',
            status: 0,
            response: 'poor'
          },
          cover: {
            image: require(`@/assets/images/assignment/current-neutral.png`),
            status: 0,
            response: 'neutral'
          }
        },
        {
          id: 2,
          article: {
            name: 'This Cute, Sleepy Puppy Will Melt Your Heart',
            content: 'Eli the labrador mix puppy is only two months old, but thanks to the Internet he’s already become a huge star! ' +
            'A short video of him falling asleep while trying to climb a flight of stairs has gone viral on social media, ' +
            'with a whopping 850,000 shares over the past two days (and counting!) His owners, the Wongs, said ‘Eli’s a very active dog. ' +
            'He must’ve been tired out after running around and he just fell asleep!’ We look forward to seeing more from Eli very soon!',
            status: 1,
            response: 'good'
          },
          cover: {
            image: require(`@/assets/images/assignment/current-poor.png`),
            status: 0,
            response: 'poor'
          }
        },
        {
          id: 3,
          article: {
            name: 'Teams Prepare to Court Soccer Star Garcia',
            content: 'Pablo Garcia’s contract is expiring at the end of the regular soccer season, and the world’s top teams are lining up to make new offers for the ace. ' +
            'Garcia, 27, has been in fine form this season, and has scored fifteen goals for Hexshall Rovers. It is widely expected that he will leave for a new team, with Italian giants Rome United leading the chase.',
            status: 0,
            response: 'neutral'
          },
          cover: {
            image: require(`@/assets/images/assignment/current-good.png`),
            status: 1,
            response: 'good'
          }
        }
      ]
    ],
    feedbacks: [
      {
        report: [
          'Well done! The readers really liked hearing about Eleanor’s adventures. Plus, the cover showed the excitement available in Mongolia and had a great headline. This issue sold like hotcakes!',
          'Oh no. The article was quite good in detailing the excitement available in Mongolia, but the cover did not attract the audience at all! It was way too plain! You’d better take note of this for your next assignment…or else!',
          'Well, the cover showed the excitement available in Mongolia and had a great headline, but the article was was so boring. It was just a collection of facts, and did not convey any of the excitement associated with taking part in activities in Mongolia. Make better choices in your next assignment…or else!',
          'Well, the cover showed the excitement available in Mongolia and had a great headline, but the article was inappropriate. It was too focused on luxury travel, which is not what our readers want. Be careful next time!',
          'Oh no. The article was quite good in detailing the excitement available in Mongolia, but the cover did not attract the audience. It made Mongolia seem expensive! You’d better take note of this for your next assignment…or else!',
          'Oh dear. The article was so boring. It was just a collection of facts, and did not convey any of the excitement associated with taking part in activities in Mongolia! Also, the cover was so dull! Who wants to see a map of the country? That will hardly convey the exciting things that can happen there! You’d better do better next time, or you’ll be looking for a new job!',
          'Oh dear. The article was so boring. It was just a collection of facts, and did not convey any of the excitement associated with taking part in activities in Mongolia! Also, the cover was poorly selected – it made Mongolia seem expensive! Make better choices in your next assignment…or else!',
          'Oh dear. The article was too focused on luxury travel, which is not what our readers want. Also, the cover was so dull! Who wants to see a map of the country? That will hardly convey the exciting things that can happen there! You’d better do better next time, or you’ll be looking for a new job!',
          'Oh dear. The article was too focused on luxury travel, which is not what our readers want. Also, the cover was poorly selected – it made Mongolia seem expensive! Make better choices in your next assignment…or else!'
        ]
      },
      {
        report: [
          'Well done! Our readers were impressed by the cover. The headlines were attention grabbing,  and suggested the content would be credible. The credibility of the magazine was further enhanced by the endorsement from the World Association of Health Sciences. The article chosen was also suitable, with a detailed description of the experiment and balanced advice from the lead researcher.',
          'Hmm! The article chosen was suitable, with a detailed description of the experiment and balanced advice from the lead researcher. However, the cover was not very suitable.  The image of the microscope was not sufficient on its own to grab the attention of health science professionals.  Think more critically next time…or else!',
          'Hmm! Our readers were impressed by the cover. The headlines were attention grabbing,  and suggested the content would be credible. The credibility of the magazine was further enhanced by the endorsement from the World Association of Health Sciences. However, the article chosen was not suitable at all.  The tone was too emotional and the article made many claims without backing them up sufficiently. Think more critically next time…or else!',
          'Hmm! Our readers were impressed by the cover. The headlines were attention grabbing,  and suggested the content would be credible. The credibility of the magazine was further enhanced by the endorsement from the World Association of Health Sciences. However, the article chosen was too vague! It was about the personal experience of the writer.',
          'Hmm! The article chosen was suitable, with a detailed description of the experiment and balanced advice from the lead researcher. However, the cover was  very poorly chosen. The headlines are more suited for a gossip magazine than a serious publication like Health Science Weekly. Think more critically next time…or else!',
          'No! The article chosen was not suitable at all.  The tone was too emotional and the article made many claims without backing them up sufficiently.  Also, the cover was  very poorly chosen. The headlines are more suited for a gossip magazine than a serious publication like Health Science Weekly. Think more critically next time…or else!',
          'No! The article chosen was not suitable at all.  The tone was too emotional and the article made many claims without backing them up sufficiently.  Also, the cover was not very suitable.  The image of the microscope was not sufficient on its own to grab the attention of health science professionals.  Think more critically next time…or else!',
          'No! The article chosen was not suitable at all.  The tone was too emotional and the article made many claims without backing them up sufficiently.  Also, the cover was  very poorly chosen. The headlines are more suited for a gossip magazine than a serious publication like Health Science Weekly. Think more critically next time…or else!)',
          'No! The article chosen was not suitable at all.  The tone was too emotional and the article made many claims without backing them up sufficiently. Also, the cover was not very suitable.  The image of the microscope was not sufficient on its own to grab the attention of health science professionals.  Think more critically next time…or else!'
        ]
      },
      {
        report: [
          'Well done! The cover was extremely uplifting and our readers reported that looking at it made them feel happy. The article was also well-chosen. Readers loved learning about the cute puppy’s antics. It definitely distracted them from the horrible things happening in the world today.',
          'Hmm! The cover was not very interesting.  The image of the bar graph suggested the magazine would be full of dry, boring statistics. This would not appeal to our readers. The article, however, was well-chosen. Readers loved learning about the cute puppy’s antics. It definitely distracted them from the horrible things happening in the world today.',
          'Hmm. The cover was extremely uplifting and our readers reported that looking at it made them feel happy. However, the article chosen was not very suitable. The story about the footballer was written in a dry, factual tone. While some people might find this interesting, it was not conveyed in an exciting enough manner.',
          'Hmm! The cover was extremely uplifting and our readers reported that looking at it made them feel happy. However, the article was very poorly chosen! Readers are stressed enough as it is, and you chose an article reminding readers about how miserable life is?',
          'Hmm! The article was well-chosen. Readers loved learning about the cute puppy’s antics. It definitely distracted them from the horrible things happening in the world today. However, very few people actually bought the magazine because the cover was very poorly chosen. You know that our readers are looking to escape their stressful lives. Having a cover that shows a frightened child does not appeal to the readers!',
          'No! The article chosen was not very suitable. The story about the footballer was written in a dry, factual tone. While some people might find this interesting, it was not conveyed in an exciting enough manner. At any rate, the cover was not very interesting.  The image of the bar graph suggested the magazine would be full of dry, boring statistics. This would not appeal to our readers!',
          'No! The article chosen was not very suitable. The story about the footballer was written in a dry, factual tone. While some people might find this interesting, it was not conveyed in an exciting enough manner. At any rate, very few people actually bought the magazine because the cover was very poorly chosen. You know that our readers are looking to escape their stressful lives. Having a cover that shows a frightened child does not appeal to the readers!',
          'The cover was not very interesting.  The image of the bar graph suggested the magazine would be full of dry, boring statistics. This would not appeal to our readers! At any rate, the article was very poorly chosen! Readers are stressed enough as it is, and you chose an article reminding readers about how miserable life is?',
          'The article was very poorly chosen! Readers are stressed enough as it is, and you chose an article reminding readers about how miserable life is? Furthermore, very few people actually bought the magazine because the cover was very poorly chosen. You know that our readers are looking to escape their stressful lives. Having a cover that shows a frightened child does not appeal to the readers!'
        ]
      }
    ],
    assignmentResults: [
      {
        id: 1,
        image: 'good-result.png',
        boss: 'boss-good.png'
      },
      {
        id: 2,
        image: 'normal-result.png',
        boss: 'boss-normal.png'
      },
      {
        id: 3,
        image: 'bad-result.png',
        boss: 'boss-bad.png'
      }
    ],
    endings: [
      {
        id: 1,
        background: 'bad-ending.png',
        text: 'Well, at least you tried.\n' + '\n' +
        'Clearly, Critical Publishing isn\'t the right environment for you.\n' + '\n' +
        'Go learn more about critical listening, reading and viewing, and maybe some other company will hire you.\n' + '\n' +
        'Because you are fired.\n' + '\n' +
        'Good bye.'
      },
      {
        id: 2,
        background: 'normal-ending.png',
        text: 'Well, you\'ve not shown yourself to be too incompetent.\n' + '\n' +
        'I\'m keeping you on the staff of Critical for now, but clearly you need some brushing up.\n' + '\n' +
        'I\'m sending you for a training course on critical thinking skills.\n' + '\n' +
        'Ryan will cover your portfolio in the meantime.\n' + '\n' +
        'Goodbye!'
      },
      {
        id: 3,
        background: 'good-ending.png',
        text: 'Well, well.\n' + '\n' +
        'Looks like you have become somewhat successful.\n' + '\n' +
        'From Monday onwards you can have your own office.\n' + '\n' +
        'I don\'t say this often but... Keep up the good work.\n' + '\n' +
        'Maybe one day you might even have my position... But that day is a long time away!\n' + '\n' +
        'Get to work!'
      }
    ],
    currentMagazineSet: 0,
    selectedCombination: {
      article: null,
      cover: null
    },
    response: 0,
    imageResult: 0,
    seeBoss: false,
    finalReview: 0,
    finalEnding: 0,
    correctScreen: false
  },
  mutations: {
    setDeskIcon (state, index) {
      state.deskIcons[index].isEnabled = true
    },
    correctScreen (state, status) {
      state.correctScreen = status
    },
    setSelectedCombination (state, payload) {
      switch (payload.type) {
        case 'article':
          state.selectedCombination.article = payload.id
          break
        case 'cover':
          state.selectedCombination.cover = payload.id
          break
        default:
          break
      }
    },
    computeScore (state) {
      const currentSet = state.magazines[state.currentMagazineSet]

      const selectedArticle = currentSet[state.selectedCombination.article - 1].article
      const selectedCover = currentSet[state.selectedCombination.cover - 1].cover

      const statusSelectedArticle = selectedArticle.status
      const statusSelectedCover = selectedCover.status

      if (statusSelectedArticle + statusSelectedCover > 1) {
        state.finalReview += 1
      }
    },
    computeRespond (state) {
      const currentRespondSet = state.magazines[state.currentMagazineSet]

      const selectedRespondArticle = currentRespondSet[state.selectedCombination.article - 1].article
      const selectedRespondCover = currentRespondSet[state.selectedCombination.cover - 1].cover

      const selectedArticleResponse = selectedRespondArticle.response
      const selectedCoverResponse = selectedRespondCover.response

      switch (true) {
        case (selectedArticleResponse === 'good' && selectedCoverResponse === 'good'):
          state.response = 0
          state.imageResult = 1
          break
        case (selectedArticleResponse === 'good' && selectedCoverResponse === 'neutral'):
          state.response = 1
          state.imageResult = 2
          break
        case (selectedArticleResponse === 'neutral' && selectedCoverResponse === 'good'):
          state.response = 2
          state.imageResult = 2
          break
        case (selectedArticleResponse === 'poor' && selectedCoverResponse === 'good'):
          state.response = 3
          state.imageResult = 2
          break
        case (selectedArticleResponse === 'good' && selectedCoverResponse === 'poor'):
          state.response = 4
          state.imageResult = 2
          break
        case (selectedArticleResponse === 'neutral' && selectedCoverResponse === 'neutral'):
          state.response = 5
          state.imageResult = 2
          break
        case (selectedArticleResponse === 'neutral' && selectedCoverResponse === 'poor'):
          state.response = 6
          state.imageResult = 3
          break
        case (selectedArticleResponse === 'poor' && selectedCoverResponse === 'neutral'):
          state.response = 7
          state.imageResult = 3
          break
        case (selectedArticleResponse === 'poor' && selectedCoverResponse === 'poor'):
          state.response = 8
          state.imageResult = 3
          break
      }
    },
    changeCurrentSet (state) {
      state.currentMagazineSet += 1
      if (state.currentMagazineSet === 2) {
        state.seeBoss = true
      }
    },
    finalEnding (state) {
      if (state.finalReview >= 3) {
        state.finalEnding = 2
      } else if (state.finalReview > 0 && state.finalReview < 3) {
        state.finalEnding = 1
      } else {
        state.finalEnding = 0
      }
    },
    clearCurrentArticleCover (state) {
      state.selectedCombination.article = null
      state.selectedCombination.cover = null
    },
    disableIcons (state) {
      state.deskIcons[1].isEnabled = false
      state.deskIcons[2].isEnabled = false
      state.deskIcons[3].isEnabled = false
    },
    checkForDisable (state) {
      if (state.selectedCombination.article === null) {
        state.deskIcons[2].isEnabled = false
      } else if (state.selectedCombination.cover === null) {
        state.deskIcons[3].isEnabled = false
      }
    }
  },
  getters: {
    currentSet (state) {
      return state.magazines[state.currentMagazineSet]
    }
  }
})
