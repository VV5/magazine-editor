import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      store, router, localVue
    })
  })

  test('should start the app when button is clicked', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })
})
